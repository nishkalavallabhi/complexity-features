/**
 * 
 */
package main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.trees.Tree;
import features.ContentOverlapFeatures;
import features.POSTagBasedFeatures;
import features.ParseTreeBasedFeatures;
import features.PsycholingFeatures;
import features.ReferringExpressionsFeatures;
import features.TraditionalFeatures;
import features.WordBasedFeatures;
import features.WordNetBasedFeatures;
import features.WordlistsBasedFeatures;
import preprocessing.PreprocessText;

/**
 * @author sowmya
 *
 */
public class GetFeaturesForADir {

	/**
	 * @param args
	 * Purpose: Take a directory path, calculate features for all files and save as a csv file.
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		//args[0] is the directory path containing .txt files
		//args[1] is the .csv file to store the output.
		extractFeaturesForDir("/Users/sowmya/Downloads/UsefulDatasets/sampledata/","/Users/sowmya/Downloads/UsefulDatasets/sample.csv"); //Change these two paths to input dir, output csv resp, Compile the file, and run it.
	}
	
	private static void extractFeaturesForDir(String inputdirpath, String outputfilepath) throws Exception
	{
	BufferedWriter bw = new BufferedWriter(new FileWriter(outputfilepath));
		
		String header = "fileName,DISC_RefExprDefArtPerSen,DISC_RefExprDefArtPerWord,DISC_RefExprPerProPerWord,DISC_RefExprPerPronounsPerSen,DISC_RefExprPossProPerSen,"
				+ "DISC_RefExprPossProPerWord,DISC_RefExprPronounsPerNoun,DISC_RefExprPronounsPerSen,DISC_RefExprPronounsPerWord,DISC_RefExprProperNounsPerNoun,DISC_globalArgumentOverlapCount,"
				+ "DISC_globalContentWordOverlapCount,DISC_globalNounOverlapCount,DISC_globalStemOverlapCount,DISC_localArgumentOverlapCount,DISC_localContentWordOverlapCount,"
				+ "DISC_localNounOverlapCount,DISC_localStemOverlapCount,POS_adjVar,POS_advVar,POS_correctedVV1,POS_modVar,POS_nounVar,POS_numAdjectives,POS_numAdverbs,POS_numConjunct,"
				+ "POS_numDeterminers,POS_numFunctionWords,POS_numInterjections,POS_numLexicals,POS_numModals,POS_numNouns,POS_numPerPronouns,POS_numPrepositions,POS_numPronouns,"
				+ "POS_numProperNouns,POS_numVerbs,POS_numVerbsVB,POS_numVerbsVBD,POS_numVerbsVBG,POS_numVerbsVBN,POS_numVerbsVBP,POS_numVerbsVBZ,POS_numWhPronouns,POS_squaredVerbVar1,"
				+ "POS_verbVar1,POS_verbVar2,SYN_CNPerClause,SYN_CNPerTunit,SYN_ComplexTunitRatio,SYN_CoordPerClause,SYN_CoordPerTunit,SYN_DependentClauseRatio,SYN_DependentClausesPerTunit,"
				+ "SYN_MLC,SYN_MLT,SYN_TunitComplexityRatio,SYN_VPPerTunit,SYN_avgParseTreeHeightPerSen,SYN_avgSentenceLength,SYN_numClausesPerSen,SYN_numConjPPerSen,SYN_numConstituentsPerSen,"
				+ "SYN_numNPSize,SYN_numNPsPerSen,SYN_numPPSize,SYN_numPPsPerSen,SYN_numRRCsPerSen,SYN_numSBARsPerSen,SYN_numSentences,SYN_numSubtreesPerSen,SYN_numTunitsPerSen,SYN_numVPSize,"
				+ "SYN_numVPsPerSen,SYN_numWHPsPerSen,Word_BilogTTR,Word_CTTR,Word_MTLD,Word_RTTR,Word_TTR,Word_UberIndex,Word_numWords";
		File dir = new File(inputdirpath);
		PreprocessText preprocess = new PreprocessText();
		
		POSTagBasedFeatures pos = new POSTagBasedFeatures();
	    WordBasedFeatures word = new WordBasedFeatures();
     	ParseTreeBasedFeatures parse = new ParseTreeBasedFeatures();
		ContentOverlapFeatures contentfeatures = new ContentOverlapFeatures();
		ReferringExpressionsFeatures refexp = new ReferringExpressionsFeatures();
		WordlistsBasedFeatures lists = new WordlistsBasedFeatures();
		
		bw.write(header);
		bw.newLine();
		for(File f:dir.listFiles())
		{
			System.out.println("Printing for file: " + f.getName());
			String content = preprocess.getFileContent(f.toString());
			List<?> taggedParsedSentences = preprocess.preProcessFile(content);
			@SuppressWarnings("unchecked") List<List<TaggedWord>> taggedSentences = (List<List<TaggedWord>>) taggedParsedSentences.get(0);
			@SuppressWarnings("unchecked") List<Tree> parsedSentences = (List<Tree>) taggedParsedSentences.get(1);
			@SuppressWarnings("unchecked") ArrayList<String> tokenizedSentences = (ArrayList<String>) taggedParsedSentences.get(2);
			
			TreeMap<String,Double> allFeatures = new TreeMap<String,Double>();
			allFeatures.putAll(pos.getPOSTagBasedFeatures(taggedSentences));
			allFeatures.putAll(word.getWordBasedFeatures(tokenizedSentences));
			allFeatures.putAll(parse.getSyntacticComplexityFeatures(parsedSentences));
			allFeatures.putAll(contentfeatures.getOverlapFeatures(taggedSentences));
			allFeatures.putAll(refexp.getRefExpFeatures(taggedSentences));
			//String temp = f.getAbsolutePath();
			String temp = f.getName();
			int linecount = 1;
			for(String s:allFeatures.keySet()) //Prints all the features along with names.
			{
			   temp += "," + allFeatures.get(s);
			}
			bw.write(temp);
			bw.newLine();
		}
		bw.close();
		System.out.println("Finished writing all files!");
	}

}